import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.ResourceBundle;


public class helloworld {
    private static final Logger logger = LogManager.getLogger(helloworld.class);
    public static void main(String[] args) {
        BasicConfigurator.configure();
        logger.info("Hello world");
        ResourceBundle resourceBundle= ResourceBundle.getBundle("resource");
        System.out.println("profile "+resourceBundle.getString("pnameRes"));

    }
}
